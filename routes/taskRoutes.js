const express = require("express")
const router = express.Router()
const TaskController = require("../controllers/TaskController")

// Create single task
router.post("/create", (request, response) => {
	TaskController.createTask(request.body).then((result) => {
		response.send(result)
	})
})

// Get all tasks
router.get("/", (request, response) => {
	TaskController.getAllTasks().then((result) => {
		response.send(result)
	})
})

// Update a task
router.patch("/:id/update", (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})

// Delete task

router.delete("/:id/delete", (request, response) => {
	TaskController.deleteTask(request.params.id).then((result) => {
		response.send(result)
	})
})



// ACTIVITY s36 ----------------------------------------------------

// 1. Create a route for getting a specific task.
// 2. Create a controller function for retrieving a specific task.
// 3. Return the result back to the client/Postman.
// 4. Process a GET request at the /tasks/:id route using postman to get a specific task


router.get("/:id", (request, response) => {
	TaskController.getSpecificTask(request.params.id).then((result) => {
		response.send(result)
	})
})

// 5. Create a route for changing the status of a task to complete.
// 6. Create a controller function for changing the status of a task to complete.
// 7. Return the result back to the client/Postman.
// 8. Process a PUT request at the /tasks/:id/complete route using postman to update a task.


router.patch("/:id/complete", (request, response) => {
	TaskController.updateStatus(request.params.id).then((result) => {
		response.send(result)
	})
})



module.exports = router