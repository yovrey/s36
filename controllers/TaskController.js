const Task = require("../models/Task.js")

module.exports.createTask = (data) => {
	let newTask = new Task({
		name: data.name
	})
	return newTask.save().then((savedTask, error) => {
		if(error) {
			console.log(error)
			return error
		}

		return savedTask
	})
}

module.exports.getAllTasks = () => {
	return Task.find({}).then((result) => {
		return result
	})
}

module.exports.updateTask = (taskId, newData) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error)
			return error
		}
		result.name = newData.name
		result.status = newData.status

		return result.save().then((updatedTask, error) => {
			if(error) {
				console.log(error)
				return error
			}

			return updatedTask
		})
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndDelete(taskId).then((removedTask, error) => {
		if(error) {
			console.log(error)
			return error
	}
		else {

			return removedTask
		}
	})
	
}





// ACTIVITY s36

module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error)
			return error
		}
		else {
			return result
		}
	})
}

module.exports.updateStatus = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error)
			return error
		}
		else {
			let newStatus = { status: "Complete" }
			result.status = newStatus.status
			return result.save()
		}
	})
}