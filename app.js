// Importing the dependencies/modules
const express = require("express")
const mongoose = require("mongoose")
const dotenv = require("dotenv")
const taskRoute = require("./routes/taskRoutes")

dotenv.config()

// SERVER SETUP
const app = express()
const port = 3003

app.use(express.json())
app.use(express.urlencoded({extended: true}))

// MongoDB Connection
mongoose.connect(`mongodb+srv://yovrey:${process.env.MONGODB_PASSWORD}@cluster0.rrk947v.mongodb.net/S35-todo?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection
db.on("error", () => console.error("Connection error"))
db.on("open", () => console.log("We are connected to MongoDB!"))

app.use("/tasks", taskRoute)

//Server Listening
app.listen(port, () => console.log(`Server running on localhost:${port}`))



